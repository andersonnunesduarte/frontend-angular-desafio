import { Injectable, Component } from '@angular/core';
import { UserDTO, UserCreateDTO, UserUpdateDTO, ErrorMessage } from '../../objects';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from '../services/authentication.service';


@Injectable()
export class UserService {

    constructor(private http: HttpClient, authenticationService: AuthenticationService) { }

    public getMe(token: string): Observable<UserDTO> {
        var httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token, 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': '*' })
        }
        return this.http.get<UserDTO>(environment.url.urlUserMe, httpOptions);
    }

    public get(userId: number): Observable<UserDTO> {
        return this.http.get<UserDTO>(environment.url.urlUser + "/" + userId);
    }

    public getAll(): Observable<Array<UserDTO>> {
        return this.http.get<Array<UserDTO>>(environment.url.urlUser);
    }

    public save(user: UserCreateDTO): Observable<ErrorMessage> {
        return this.http.post<ErrorMessage>(environment.url.urlUser, user);

    }

    public update(user: UserUpdateDTO): Observable<ErrorMessage> {
        return this.http.put<ErrorMessage>(environment.url.urlUser + "/" + user.userId, user);
    }

    public delete(userId: number): Observable<void> {
        return this.http.delete<void>(environment.url.urlUser + "/" + userId);
    }
}
