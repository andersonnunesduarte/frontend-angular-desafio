import { Injectable, Component } from '@angular/core';
import { CarDTO, ErrorMessage } from '../../objects';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from './authentication.service';


@Injectable()
export class CarService {

    constructor(private http: HttpClient, authenticationService: AuthenticationService) { }

    public getByToken(token: string): Observable<CarDTO> {
        var httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token, 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': '*' })
        }
        return this.http.get<CarDTO>(environment.url.urlUserMe, httpOptions);
    }

    public get(carId: number): Observable<CarDTO> {
        return this.http.get<CarDTO>(environment.url.urlCar + "/" + carId);
    }

    public getAll(token: string): Observable<Array<CarDTO>> {
        var httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token, 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': '*' })
        }
        return this.http.get<Array<CarDTO>>(environment.url.urlCar, httpOptions);
    }

    public save(token: string, car: CarDTO): Observable<ErrorMessage> {
        var httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token, 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': '*' })
        }
        return this.http.post<ErrorMessage>(environment.url.urlCar, car, httpOptions);
    }

    public delete(token: string, carId: number): Observable<void> {
        var httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token, 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': '*' })
        }
        return this.http.delete<void>(environment.url.urlCar + "/" + carId, httpOptions);
    }
}
