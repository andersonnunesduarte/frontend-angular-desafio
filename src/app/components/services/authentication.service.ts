import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { LoginDTO, Token } from '../../objects';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tokenName } from '@angular/compiler';

@Injectable()
export class AuthenticationService {

    constructor(private http: HttpClient, private router: Router) { }

    public login(login: LoginDTO): Observable<Token> {
        return this.http.post<Token>(environment.url.urlLogin, login);
    }

    public saveSession(session: Token) {
        localStorage.setItem("mpManagerToken", JSON.stringify(session));
    }

    public getSession(): Token {
        console.log("TOKEN:" + localStorage.getItem("mpManagerToken"));
        let session: string =  localStorage.getItem("mpManagerToken");
        let token :Token = JSON.parse(session);
        return token;
    }

    public isLogged(): boolean {
        return this.getSession() != null;
    }

    public logout() {  
        localStorage.removeItem("mpManagerToken");
        this.router.navigate(['login']);
    }
}