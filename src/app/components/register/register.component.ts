import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserCreateDTO } from '../../objects';
import { UserService } from '../services/user.service';
import { MatSnackBar } from '@angular/material';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  message: string;
  showSpinner = false;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private snackBar: MatSnackBar) { }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      birthday: ['', Validators.required],
      phone: ['', Validators.required],
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() { return this.registerForm.controls; };

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.userService.save(new UserCreateDTO(null, this.f.firstname.value, this.f.lastname.value, this.f.email.value, //
      this.f.birthday.value, this.f.phone.value, this.f.login.value, this.f.password.value)).subscribe(response => {
        console.log(response);
        this.resetForm(this.registerForm);
        this.openSnackBar("Sucess! Register sucessfull.", 'X');
      }, error => {
        this.showSpinner = false;
        if (error != null) {
          this.message = error.error.message;
        } else {
          this.message = "Error! Try Again.";
        }
        this.openSnackBar(this.message, 'X');
      });
  }

  resetForm(form: FormGroup) {
    form.reset();
    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
    });
  }
}