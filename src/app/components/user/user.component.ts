import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { UserDTO, UserUpdateDTO, Token } from '../../objects';
import { AuthenticationService } from '../services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  userForm: FormGroup;
  public user: UserDTO = new UserDTO();
  showSpinner = false;
  submitted = false;
  message: string;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router,
    private userService: UserService, private authenticationService: AuthenticationService, private snackBar: MatSnackBar) { }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  ngOnInit() {
    this.getUser();
    if (this.user.birthday != null) {
      this.user.birthday = new Date(this.user.birthday);
    } else {
      this.user.birthday = null;
    }
    this.userForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      birthday: ['', Validators.required],
      phone: ['', Validators.required],
    });
  }

  getUser() {
    this.showSpinner = true;
    let token: Token = this.authenticationService.getSession();
    console.log(token.token);
    this.userService.getMe(token.token).subscribe(retorno => {
      this.user = retorno;
      this.showSpinner = false;
    }, error => {
      this.showSpinner = false;
      if (error != null) {
        this.message = error.error.message;
      } else {
        this.message = "Error! Try Again!";
      }
      this.openSnackBar(this.message, 'X');
    });
  }

  get f() { return this.userForm.controls; };

  onSubmit() {
    this.submitted = true;

    if (this.userForm.invalid) {
      return;
    }

    this.userService.update(new UserUpdateDTO(this.user.userId, this.f.firstname.value, this.f.lastname.value, this.f.email.value, //
      this.f.birthday.value, this.f.phone.value)).subscribe(response => {
        console.log(response);
        this.userForm.reset;
        this.resetForm(this.userForm);
        this.openSnackBar("Sucess! User update", 'X');
        this.getUser();
      }, error => {
        this.showSpinner = false;
        if (error != null) {
          this.message = error.error.message;
        } else {
          this.message = "Error! Try Again!";
        }
        this.openSnackBar(this.message, 'X');
      });
  }

  resetForm(form: FormGroup) {
    form.reset();
    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
    });
  }

  logout() {
    localStorage.clear;
    sessionStorage.clear;
    this.router.navigate(["/login"]);
    this.openSnackBar("Logout Sucess!", 'X');
  }
}
