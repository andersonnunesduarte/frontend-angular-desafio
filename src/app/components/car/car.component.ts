import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CarService } from '../services/car.service';
import { CarDTO, Token } from '../../objects';
import { AuthenticationService } from '../services/authentication.service';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';


@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})

export class CarComponent implements OnInit {
  cars: Array<CarDTO> = [];
  showSpinner = false;
  displayedColumns: string[] = ['year', 'licensePlate', 'model', 'color', 'actions'];
  dataSource: Array<CarDTO> = [];
  message: string = null;

  constructor(private route: ActivatedRoute, private router: Router,
    private carService: CarService, private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar) { }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  ngOnInit() {
    this.getCar();
  }

  getCar() {
    this.showSpinner = true;
    let token: Token = this.authenticationService.getSession();
    this.carService.getAll(token.token).subscribe(retorno => {
      this.dataSource = retorno;
      this.showSpinner = false;
    }, error => {
      this.showSpinner = false;
      if (error.error != null && error.error.msg != null) {
        this.message = error.error.msg;
      } else {
        this.message = "Error! Try Again!";
      }
      this.openSnackBar(this.message, 'X');
    });
  }

  deleteCar(car: CarDTO) {
    this.showSpinner = true;
    let token: Token = this.authenticationService.getSession();
    this.carService.delete(token.token, car.carId).subscribe(retorno => {
      this.getCar();
      this.openSnackBar("Sucess! Car deleted.", 'X');
    }, error => {
      this.showSpinner = false;
      if (error.error != null && error.error.message != null) {
        this.message = error.error.msg;
      } else {
        this.message = "Error! Try Again!";
      }
      this.openSnackBar(this.message, 'X');
    });
  }

  logout() {
    localStorage.clear;
    sessionStorage.clear;
    this.router.navigate(["/login"]);
    this.openSnackBar("Logout Sucess!", 'X');
  }

  newcar(){
    this.router.navigate(['new-car']);
  }

}
