import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { LoginDTO } from '../../objects';
import { MatSnackBar } from '@angular/material';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  login: LoginDTO;
  errorMessage: string;
  showSpinner = false;
  message: string;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar,
    private routerModele: RouterModule) {
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.showSpinner = true;
    this.authenticationService.login(new LoginDTO(this.f.username.value, this.f.password.value)).subscribe(retorno => {
      this.authenticationService.saveSession(retorno);
      this.openSnackBar("Sucess! Login ok.", 'X');
      this.router.navigate(['user']);
    }, error => {
      if (error != null) {
        this.message = error.error.message;
      } else {
        this.message = "Error! Try Again.";
      }
      console.log(error);
      this.openSnackBar(this.message, 'X');
      this.showSpinner = true;
    });
  }

}
