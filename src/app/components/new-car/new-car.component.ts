import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CarService } from '../services/car.service';
import { CarDTO, Token } from '../../objects';
import { AuthenticationService } from '../services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-new-car',
  templateUrl: './new-car.component.html',
  styleUrls: ['./new-car.component.css']
})
export class NewCarComponent implements OnInit {
  carForm: FormGroup;
  public user: CarDTO = new CarDTO();
  showSpinner = false;
  submitted = false;
  message: string;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router,
    private carService: CarService, private authenticationService: AuthenticationService, private snackBar: MatSnackBar) { }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  ngOnInit() {
    this.carForm = this.formBuilder.group({
      year: ['', Validators.required],
      licensePlate: ['', Validators.required],
      model: ['', Validators.required],
      color: ['', Validators.required],
    });
  }

  get f() { return this.carForm.controls; };

  onSubmit() {
    this.submitted = true;

    if (this.carForm.invalid) {
      return;
    }
    let token: Token = this.authenticationService.getSession();
    this.carService.save(token.token, new CarDTO(null, this.f.year.value, this.f.licensePlate.value, this.f.model.value, //
      this.f.color.value)).subscribe(response => {
        this.openSnackBar("Sucess! New car insert", 'X');
        this.router.navigate(["/car"]);
      }, error => {
        this.showSpinner = false;
        if (error != null) {
          this.message = error.error.message;
        } else {
          this.message = "Error! Try Again!";
        }
        this.openSnackBar(this.message, 'X');
      });
  }

  logout() {
    localStorage.clear;
    sessionStorage.clear;
    this.router.navigate(["/login"]);
    this.openSnackBar("Logout Sucess!", 'X');
  }

}
