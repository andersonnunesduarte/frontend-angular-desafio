export class UserDTO {
  userId: number;
  firstName: string;
  lastName: string;
  email: string;
  birthday: Date;
  login: string;
  password: string;
  phone: string;
  cars: Array<CarDTO>;
}

export class UserCreateDTO {
  userId: number;
  firstName: string;
  lastName: string;
  email: string;
  birthday: Date;
  phone: string;
  login: string;
  password: string;

  constructor(userId: number, firsName: string, lastName: string, email: string, birthday: Date, phone: string, login: string, password: string) {
    this.userId = userId;
    this.firstName = firsName;
    this.lastName = lastName;
    this.email = email;
    this.birthday = birthday;
    this.phone = phone;
    this.login = login;
    this.password = password;
  }
}

export class UserUpdateDTO {
  userId: number;
  firstName: string;
  lastName: string;
  email: string;
  birthday: Date;
  phone: string;

  constructor(userId: number, firsName: string, lastName: string, email: string, birthday: Date, phone: string) {
    this.userId = userId;
    this.firstName = firsName;
    this.lastName = lastName;
    this.email = email;
    this.birthday = birthday;
    this.phone = phone;
  }
}

export class CarDTO {
  carId: number;
  year: string;
  licensePlate: string;
  model: string;
  color: string;
  constructor(id?: number, year?: string, licensePlate?: string, model?: string, color?: string) {
    this.carId = this.carId;
    this.year = year;
    this.licensePlate = licensePlate;
    this.model = model;
    this.color = color;
  }
}

export class LoginDTO {
  login: String;
  password: String;

  constructor(login: String, password: String) {
    this.login = login;
    this.password = password;
  }
}

export class Token {
  token: string;
  constructor(token: string) {
    this.token = token;
  }
}

export class ErrorMessage {
  status: number;
  message: string;
  constructor(status: number, message: string) {
    this.status = status;
  }
}

export class Erro {
  campo: string;
  valorInformado: string;
  erro: string;
  constructor(campo: string, valorInformado: string, erro: string) {
    this.campo = campo;
    this.valorInformado = valorInformado;
    this.erro = erro;
  }
}
