import { environment } from './environment';

export class BackEndURLs {

    urlServer: String;

    urlLogin;
    urlUser;
    urlCar;
    urlUserMe;

    constructor(urlServer: String) {
        this.urlServer = urlServer;
        this.urlLogin = this.urlServer + "/user/api/signin";
        this.urlCar = this.urlServer + "/user/api/cars";
        this.urlUser = this.urlServer + "/user/api/users"
        this.urlUserMe = this.urlServer + "/user/api/me";
     }

}
