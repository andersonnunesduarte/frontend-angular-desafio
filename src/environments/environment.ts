import { BackEndURLs } from "./backEndURLs";

export const environment = {    
  production: true,
  url: new BackEndURLs("http://localhost:8080")  
 };